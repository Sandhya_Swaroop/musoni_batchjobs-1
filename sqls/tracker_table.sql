-- auto-generated definition
create table musoni_payment_update_tracker
(
    confirmation_code varchar   not null,
    status            varchar   not null,
    created_dt        timestamp not null,
    reason            varchar,
    updated_dt        timestamp default now(),
    id                serial
);